*In order to try these snippets you need Ruby 2.x*

## Funky Flatten
Usage:
```ruby
require './flatten/flatten'
include FunkyFlatten

funky_flatten([1,[2,[3]],4])
=> [1, 2, 3, 4]
```
You can also extend the Array class:
```ruby
class Array
  include FunkyFlatten
end

[1,[2,[3]],4].funky_flatten
=> [1, 2, 3, 4]
```

## Interplanetary Messenger

Type your distress message with:
```ruby
require './morse/interplanetary_messenger'
InterplanetaryMessenger.type_message

Please type your distress message: Testing the messenger.
Obfuscated message: A|1|3|A|2|A1|B1/A|4|1/B|1|3|3|1|A1|B1|1|1A1|1A1A1A
```

Read message from a file:
```ruby
InterplanetaryMessenger.read_file('input.txt', 'output.txt')
```

### Morse Encoder

You can use the morse encoder in isolation:
```ruby
require './morse/encoder'
Morse::Encoder.run('I AM IN TROUBLE')
=> "../.-|--/..|-./-|.-.|---|..-|-...|.-..|."
```

### Morse Obfuscator

It's also possible to use the obfuscator in isolation:
```ruby
require './morse/obfuscator'
Morse::Obfuscator.run('../.-|--/..|-./-|.-.|---|..-|-...|.-..|.')
=> "2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1"
```

## Running the specs

```bash
$ gem install bundler
$ bundle install
$ bundle exec rspec
```
