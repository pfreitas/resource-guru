require_relative '../morse/obfuscator'

RSpec.describe Morse::Obfuscator do
  describe '.run' do
    let(:morse_message) { '../.-|--/..|-./-|.-.|---|..-|-...|.-..|.' }
    let(:obfuscated_message) { '2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1' }

    subject { described_class.run(morse_message) }

    it 'returns the obfuscated message' do
      expect(subject).to eq obfuscated_message
    end

    context 'when the message is not a String' do
      let(:morse_message) { [] }

      it 'raises an ArgumentError' do
        expect { subject }.to raise_error(ArgumentError)
      end
    end

    context 'when the message is empty' do
      let(:morse_message) { '' }

      it 'returns an empty message' do
        expect(subject).to eq ''
      end
    end

    context 'when the message has unexpected characters' do
      let(:morse_message) { '../,|--' }

      it 'raises an InvalidMessage error' do
        expect { subject }.to raise_error(Morse::Obfuscator::InvalidMessage)
      end
    end

    context 'when the message has too many dots in a row' do
      let(:morse_message) { '../......|--' }

      it 'raises an InvalidMessage error' do
        expect { subject }.to raise_error(Morse::Obfuscator::InvalidMessage)
      end
    end

    context 'when the message has too many dashes in a row' do
      let(:morse_message) { '../------|--' }

      it 'raises an InvalidMessage error' do
        expect { subject }.to raise_error(Morse::Obfuscator::InvalidMessage)
      end
    end
  end
end
