require_relative '../morse/encoder'

RSpec.describe Morse::Encoder do
  describe '.run' do
    let(:message) { 'I AM IN TROUBLE' }
    let(:encoded_message) { '../.-|--/..|-./-|.-.|---|..-|-...|.-..|.' }

    subject { described_class.run(message) }

    it 'returns the encoded message' do
      expect(subject).to eq encoded_message
    end

    context 'when the message has unsupported characters' do
      let(:message) { 'HELP!' }

      it 'raises an error' do
        expect { subject }.to raise_error(Morse::Encoder::UnsupportedCharacter)
      end
    end

    context 'when the message has multiple spaces in a row' do
      let(:message) { 'I  AM   IN    TROUBLE' }

      it 'treats them as a single space' do
        expect(subject).to eq encoded_message
      end
    end

    context 'when the message has lower case characters' do
      let(:message) { 'I am in trouble' }

      it 'treats them as upper case characters' do
        expect(subject).to eq encoded_message
      end
    end

    context 'when the message contains all the possible characters' do
      let(:message) { 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,' }

      it 'encodes every character correctly' do
        expect(subject).to eq '.-|-...|-.-.|-..|.|..-.|--.|....|..|.---|-.-|.-..|--|-.|---|.--.|--.-|.-.|...|-|..-|...-|.--|-..-|-.--|--..|-----|.----|..---|...--|....-|.....|-....|--...|---..|----.|.-.-.-|--..--'
      end
    end
  end
end
