require_relative '../morse/interplanetary_messenger'

RSpec.describe InterplanetaryMessenger do
  describe '.type_message' do
    let(:message) { 'I AM IN TROUBLE' }
    let(:encoded_message) { '../.-|--/..|-./-|.-.|---|..-|-...|.-..|.' }
    let(:obfuscated_message) { '2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1' }

    subject { described_class.type_message }

    before do
      # Stub STDIN and STDOUT
      allow(described_class).to receive(:print)
      allow(described_class).to receive(:puts)
      allow(described_class).to receive(:gets).and_return(message)
      # We could stub the services if we wanted to test this in isolation
      # allow(Morse::Encoder).to receive(:run).and_return(encoded_message)
      # allow(Morse::Obfuscator).to receive(:run).and_return(obfuscated_message)
    end

    it 'reads input from STDIN' do
      expect(described_class).to receive(:gets)
      subject
    end

    it 'calls the Morse::Encoder' do
      expect(Morse::Encoder).to receive(:run).with(message).and_call_original
      subject
    end

    it 'calls the Morse::Obfuscator' do
      expect(Morse::Obfuscator).to receive(:run).with(encoded_message)
      subject
    end

    it 'prints the obfuscated message' do
      expect(described_class).to receive(:puts).with(/#{obfuscated_message}/)
      subject
    end

    context 'when the message cannot be encoded' do
      before do
        allow(Morse::Encoder)
          .to receive(:run)
          .and_raise(Morse::Obfuscator::InvalidMessage)
      end

      it 'returns an error message' do
        expect(described_class)
          .to receive(:puts)
          .with(InterplanetaryMessenger::UNPROCESSABLE_ERROR_MESSAGE)

        subject
      end
    end
  end

  describe '.read_file' do
    let(:input_file_path) { 'spec/fixtures/input.txt' }
    let(:output_file_path) { 'spec/fixtures/output.txt' }
    let(:encoded_message) { '../.-|--/..|-./-|.-.|---|..-|-...|.-..|.' }
    let(:obfuscated_message) { '2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1' }

    subject { described_class.read_file(input_file_path, output_file_path) }

    after do
      File.delete(output_file_path) if File.exists?(output_file_path)
    end

    it 'calls the Morse::Encoder for each line in the file' do
      expect(Morse::Encoder)
        .to receive(:run)
        .with('HELLO')
        .and_call_original

      expect(Morse::Encoder)
        .to receive(:run)
        .with('I AM IN TROUBLE')
        .and_call_original

      subject
    end

    it 'calls the Morse::Obfuscator for each line in the file' do
      expect(Morse::Obfuscator)
        .to receive(:run)
        .with('....|.|.-..|.-..|---')

      expect(Morse::Obfuscator)
        .to receive(:run)
        .with('../.-|--/..|-./-|.-.|---|..-|-...|.-..|.')

      subject
    end

    it 'writes the obfuscated message to the output file' do
      subject
      expect(File.read(output_file_path))
        .to eq "4|1|1A2|1A2|C\n2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1\n"
    end

    context 'when the message cannot be encoded' do
      before do
        allow(Morse::Encoder)
          .to receive(:run)
          .and_raise(Morse::Obfuscator::InvalidMessage)
      end

      it 'returns an error message' do
        expect(described_class)
          .to receive(:puts)
          .with(InterplanetaryMessenger::UNPROCESSABLE_ERROR_MESSAGE)

        subject
      end
    end

    context 'when the input file is invalid' do
      let(:input_file_path) { 'spec/fixtures/invalid-input.txt' }

      it 'returns an error message' do
        expect(described_class)
          .to receive(:puts)
          .with(InterplanetaryMessenger::INVALID_FILE_ERROR_MESSAGE)

        subject
      end
    end

    context 'when the output directory is invalid' do
      let(:output_file_path) { 'spec/invalid/output.txt' }

      it 'returns an error message' do
        expect(described_class)
          .to receive(:puts)
          .with(InterplanetaryMessenger::INVALID_FILE_ERROR_MESSAGE)

        subject
      end
    end
  end
end
