require_relative '../flatten/flatten'
include FunkyFlatten

RSpec.describe FunkyFlatten do
  describe '#funky_flatten' do

    subject { funky_flatten(ary) }

    context 'when parameter is not an array' do
      let(:ary) { :something_else }

      it 'raises an Argument error' do
        expect { subject }.to raise_error(ArgumentError)
      end
    end

    context 'when passed an empty array' do
      let(:ary) { [] }

      it 'should return an empty array' do
        expect(subject).to eq ary
      end
    end

    context 'when passed a flat array' do
      let(:ary) { [ 1, 2, 3, 4 ] }

      it 'should return a copy of the array' do
        expect(subject).to eq ary
      end
    end

    context 'when passed a nested array' do
      let(:ary) { [ 1, [ 2, [ 3 ] ], 4 ] }

      it 'should flatten it' do
        expect(subject).to eq [ 1, 2, 3, 4 ]
      end
    end

    context 'when passed an array with empty arrays' do
      let(:ary) { [ 1, [ [], 2 ], [ 3, [] ], 4 ] }

      it 'should flatten it' do
        expect(subject).to eq [ 1, 2, 3, 4 ]
      end
    end

    context 'when included in the Array class' do
      before do
        class Array
          include FunkyFlatten
        end
      end

      let(:ary) { [ 1, [ 2, [ 3 ] ], 4 ] }

      subject { ary.funky_flatten }

      it 'flattens itself' do
        expect(subject).to eq [ 1, 2, 3, 4 ]
      end
    end
  end
end
