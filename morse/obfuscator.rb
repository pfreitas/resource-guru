module Morse
  class Obfuscator
    InvalidMessage = Class.new(StandardError)

    ASCII_OFFSET = 64

    def self.run(message)
      new(message).run
    end

    def initialize(message)
      @message = message
    end

    def run
      raise ArgumentError unless message.is_a? String
      raise InvalidMessage unless valid_message?

      message.gsub(/\.+/) do |dots|
        dots.length
      end.gsub(/-+/) do |dashes|
        (ASCII_OFFSET + dashes.length).chr
      end
    end

    private

    attr_accessor :message

    def valid_message?
      # We should only encode up to 5 five dots in a row
      message.scan(/\.+/) do |dots|
        return false if dots.length > 5
      end
      # We should only encode up to 5 five dashes in a row
      message.scan(/-+/) do |dashes|
        return false if dashes.length > 5
      end
      # We only support these 4 characters: . - | /
      !message.match(/[^\.\-\|\/]/)
    end
  end
end
