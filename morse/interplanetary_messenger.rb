require_relative './encoder'
require_relative './obfuscator'

class InterplanetaryMessenger
  UNPROCESSABLE_ERROR_MESSAGE = 'We could not process your message. Make sure you only use alphanumeric characters.'.freeze
  INVALID_FILE_ERROR_MESSAGE = 'Invalid file path provided. Make sure your input file and output directory both exist.'.freeze

  class << self
    def type_message
      print 'Please type your distress message: '
      obfuscated = process_message(gets)
      puts "Obfuscated message: #{obfuscated}"
    rescue Morse::Obfuscator::InvalidMessage
      puts UNPROCESSABLE_ERROR_MESSAGE
    end

    def read_file(input_file_path, output_file_path)
      File.open(output_file_path, 'w') do |output|
        File.read(input_file_path).each_line do |message|
          output.puts process_message(message)
        end
      end
    rescue Errno::ENOENT
      puts INVALID_FILE_ERROR_MESSAGE
    rescue Morse::Obfuscator::InvalidMessage
      puts UNPROCESSABLE_ERROR_MESSAGE
    end

    private

    def process_message(message)
      message.chomp!
      morse = Morse::Encoder.run(message)
      Morse::Obfuscator.run(morse)
    end
  end
end
