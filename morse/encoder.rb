module Morse
  class Encoder
    UnsupportedCharacter = Class.new(StandardError)

    def self.run(message)
      new(message).run
    end

    def initialize(message)
      @message = message
    end

    def run
      raise UnsupportedCharacter unless valid_message?

      message.split(/\s+/).map do |word|
        encode_word word
      end.join(WORD_SEPARATOR)
    end

    private

    attr_accessor :message

    # If there's an efficiency concern, we could do this directly inside
    # `encode_word`. IMO making it explicit keeps the code more readable.
    def valid_message?
      message.chars.all? { |char| char == ' ' || MAPPINGS[char.upcase] }
    end

    def encode_word(word)
      word.upcase.chars.map do |char|
        MAPPINGS[char]
      end.join(CHAR_SEPARATOR)
    end
  end

  WORD_SEPARATOR = '/'.freeze
  CHAR_SEPARATOR = '|'.freeze
  MAPPINGS = {
    'A' => '.-',
    'B' => '-...',
    'C' => '-.-.',
    'D' => '-..',
    'E' => '.',
    'F' => '..-.',
    'G' => '--.',
    'H' => '....',
    'I' => '..',
    'J' => '.---',
    'K' => '-.-',
    'L' => '.-..',
    'M' => '--',
    'N' => '-.',
    'O' => '---',
    'P' => '.--.',
    'Q' => '--.-',
    'R' => '.-.',
    'S' => '...',
    'T' => '-',
    'U' => '..-',
    'V' => '...-',
    'W' => '.--',
    'X' => '-..-',
    'Y' => '-.--',
    'Z' => '--..',
    '0' => '-----',
    '1' => '.----',
    '2' => '..---',
    '3' => '...--',
    '4' => '....-',
    '5' => '.....',
    '6' => '-....',
    '7' => '--...',
    '8' => '---..',
    '9' => '----.',
    '.' => '.-.-.-',
    ',' => '--..--'
  }.freeze
end
