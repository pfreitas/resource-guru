module FunkyFlatten
  def funky_flatten(ary = self)
    raise ArgumentError unless ary.is_a? Array

    flat_ary = []
    ary.each do |el|
      if el.is_a? Array
        flat_ary += funky_flatten(el)
      else
        flat_ary << el
      end
    end

    flat_ary
  end
end
